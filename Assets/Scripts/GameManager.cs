using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager> {

    [Header("Managers")]
    public GameObject playerPrefab;
    public GameObject mapManagerPrefab;
    public GameObject cameraManagerPrefab;
    public GameObject audioManagerPrefab;
    public GameObject postProcessManagerPrefab;
    public GameObject uiManagerPrefab;

    [HideInInspector]
    public Player player;
    [HideInInspector]
    public MapManager map;
    [HideInInspector]
    public CameraManager cameraManager;
    [HideInInspector]
    public AudioManager audioManager;
    [HideInInspector]
    public PostProcessManager postProcessManager;
    [HideInInspector]
    public TimerManager timerManager;
    [HideInInspector]
    public UIManager uiManager;
    [HideInInspector]
    public InputManager inputManager;
    [HideInInspector]
    public GameObject managerFolder;
    [HideInInspector]
    public bool partieDejaTerminee = false;
    protected bool isPaused = false;
    protected bool initializationIsOver = false;
    [HideInInspector]
    public UnityEvent onInitilizationFinish;
    [HideInInspector]
    public UnityEvent onFirstFrame;

    void Start()
    {
        managerFolder = new GameObject("Managers");
        timerManager = TimerManager.Instance;
        map = Instantiate(mapManagerPrefab, managerFolder.transform).GetComponent<MapManager>();
        player = Instantiate(playerPrefab).GetComponent<Player>();
        cameraManager = Instantiate(cameraManagerPrefab, managerFolder.transform).GetComponent<CameraManager>();
        audioManager = Instantiate(audioManagerPrefab, managerFolder.transform).GetComponent<AudioManager>();
        postProcessManager = Instantiate(postProcessManagerPrefab, managerFolder.transform).GetComponent<PostProcessManager>();
        uiManager = Instantiate(uiManagerPrefab, managerFolder.transform).GetComponent<UIManager>();
        inputManager = InputManager.Instance;

        Initialize();
    }


    protected virtual void Initialize()
    {
        map.Initialize();
        player.Initialize();
        cameraManager.Initialize();
        audioManager.Initialize();
        postProcessManager.Initialize();
        uiManager.Initialize();
        FinishInitialization();
    }

    private void FinishInitialization()
    {
        initializationIsOver = true;
        onInitilizationFinish.Invoke();
        CallEventsOneFrameAfterFinishInitialization();
    }

    protected void CallEventsOneFrameAfterFinishInitialization() {
        StartCoroutine(CCallEventsOneFrameAfterFinishInitialization());
    }

    protected IEnumerator CCallEventsOneFrameAfterFinishInitialization() {
        yield return null;
        onFirstFrame.Invoke();
    }

    void Update() {
        CheckRestartGame();

        CheckPauseToggling();
    }

    protected void CheckPauseToggling() {
        if (inputManager.GetPauseGame()) {
            if (!isPaused) {
                Pause();
            }
            else
            {
                UnPause();
            }
        }
    }

    public void Pause() {
        isPaused = true;
        Time.timeScale = 0.0f;
        audioManager.PauseSounds();
    }

    public void UnPause() {
        isPaused = false;
        Time.timeScale = 1.0f;
        audioManager.UnPauseSounds();
    }

    public bool IsPaused() {
        return isPaused;
    }

    protected void CheckRestartGame()
    {
        if (inputManager.GetRestartGame())
        {
            RestartGame();
        }
        if (inputManager.GetQuitGame())
        {
            QuitGame();
        }
    }

    public void RestartGame()
    {
        Time.timeScale = 1.0f;
        string sceneName = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(sceneName);
    }

    public void QuitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    public bool IsInitializationOver()
    {
        return initializationIsOver;
    }
}


